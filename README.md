### Hi, I'm Shubham Kumar 👋

I love to contribute to open-source projects. I also write about software engineering, learning, and career **to help readers.** I am based in Berlin, Germany. I previously worked as **Google Summer of Code 2021 Developer at [GitLab](https://gitlab.com)** and **Google Summer of Code 2020 Developer at [@Mozilla](https://github.com/mozilla)**. 

<!-- <img align="right" alt="GIF" height="300px" width="300px" src="./assets/skr-sig.gif" /> -->

- 🔭 I’m currently working on backend development projects.
- 🌱 I’m currently learning Go, Ruby on Rails, and helping the tech community through my [writing](https://medium.com/@imskr).
- 👯 I’m willing to collaborate on building communities, and open source projects.
- 📫 How to reach me: shubhamkrai123@gmail.com
- 😄 Pronouns: He/Him.
- **📫 Newsletter: [Join](https://relentless-hustler-6722.ck.page/410dad83c2)**

- **Youtube: [Subscribe](https://www.youtube.com/channel/UC7-FIPYuGBxD5RzInWIXF4w)**


<br>
<a href="https://twitter.com/TheTweetOfSKR">
  <img align="left" alt="Shubham Kumar | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />
</a>&emsp;

<a href="https://www.linkedin.com/in/imskr/">
  <img align="left" alt="Shubham's LinkdeIN" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
</a>&emsp;

<a href="https://imskr.medium.com">
  <img align="left" alt="Shubham's Blog" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/medium.svg" />
</a>&emsp;


<a href="https://www.buymeacoffee.com/imskr">
  <img align="left" alt="Buy me a Coffee" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/buymeacoffee.svg" />
</a>&emsp;

<a href="https://github.com/imskr">
  <img align="left" alt="Shubham Kumar | GitLab" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/github.svg" />
</a>&emsp;
